# Normal operation

```mermaid
graph TB
  
  Browser -->|1. HTTPS request | Application
  Application --> |2. Ping request| Google
  Google --> |3. Ping response| Application
  Application --> |4. HTTPS response| Browser

  subgraph Workstation
    Browser
    end

  subgraph Azure
    subgraph Kubernetes
      subgraph Container1
        Application
        end
      end
    end
```

# Hack attempt

```mermaid
graph TB
  
  Browser -->|1. HTTPS request| Application
  Application --> |2. Ping request| Google
  Google --> |3. Ping response| Application
  Application --> |4. Run command | RedisCommand
  RedisCommand --> |5. Redis connect| Redis
  Redis --> |6. Redis response| Application
  Application --> |7. HTTPS response| Browser

  subgraph Workstation
    Browser
    end

  subgraph Azure
    subgraph Kubernetes
      subgraph Container1
        Application
        RedisCommand
        end
      subgraph Container2
        Redis
        end
      end
    end
```


# Container security


```mermaid
graph TB


subgraph CloudProvider
  subgraph Kubernetes
    subgraph NetworkSecurityPolicy
      subgraph HostSecurityPolicy
        subgraph Container
          Application
          end
        end
      end
      end
end
```
