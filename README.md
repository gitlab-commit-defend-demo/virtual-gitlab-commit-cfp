# Virtual Gitlab Commit CFP

Our working project for our [GitLab Commit 2020 CFP](https://about.gitlab.com/events/commit)

---

Abstract: 

## Your Attackers Won't Be Happy! How GitLab Can Help You Secure Your Applications

Join Nico, Philippe, and Wayne for a session full of Containers, Kubernetes, and security!
 
In this talk, you will learn how GitLab Defend features can help you to effectively secure your applications and services. We will guide you through different features using real-scenarios use-cases and demos with demonstrations from the perspectives of a software developer, security engineer, and hacker. You will learn how to secure your application ingress using the Web Application Firewall, securing east-west application traffic with Container Network Security as well as threat detection with Container Behaviour Analytics. Everything within your GitLab project!
 
Walk away and know everything you need to know to successfully secure your applications and services!

---

Detailed [Demo scenario](scenario.md)