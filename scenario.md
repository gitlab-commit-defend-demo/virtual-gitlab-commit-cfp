This is the detailed scenario of our Virtual Gitlab Commit demo. This series of events are based on our [slides](#3).

## [Nico/Wayne/Philippe] **Intro**
Nico will share his screen

  - Nico will do a short intro
  - We will introduce ourselves
  - Wayne will introduce the roles

## [Nico] **Normal application use**
Nico will still share his screen

  - Introduce the app (slides)
  - Load https://app.commit.meisenzahl.org/ in browser
  - enter `localhost` in the application field, explain the results

## [Wayne] **Attack the application!**
Wayne will share his screen

  - Exploit the application (slides)
  - Load https://app.commit.meisenzahl.org/ in browser
  - Enter `localhost > /dev/null ; redis-cli -h redis-master.redis SET key0 'All your base are belong to us'` in the input field, submit
  - Repeat with `localhost >/dev/null; redis-cli -h redis-master.redis GET key0` to show we can read the key as expected

## [Philippe] **Detect the attacks**
Philippe will share his screen
  - WAF: confirm in term `kubectl logs -n gitlab-managed-apps -l app=nginx-ingress,component=controller -c modsecurity-log -f --since=1s | fgrep --line-buffered '"host":"app.commit.meisenzahl.org"' | jq .transaction.messages`
  - Falco: in term `kubectl -n gitlab-managed-apps logs -l app=falco -f --since=1s | fgrep --line-buffered production-app | jq`
  - Layered security controles (slides)
  - Talk about Network Policies and why we implement WAF (stop attack before it access the cluster)
  
## [Philippe] **Fix the security issue**
Philippe will share his screen

  - Ask Nico to set WAF to blocking mode
  - Ask Nico to enable Network Policy
  - Ask Nico to stop leaving RCE holes in his app

## [Nico] **Fix the security issue - bis**
Nico will share his screen

  - Enable Network Polcies via UI
  - Test Network Polcies
  - Enable WAF blocking mode via UI
  - Test blocking mode
  - Use SAST to identify the issue
  - Create an issue to fix the root cause

## [Wayne] **Closing**
Wayne will share his screen

  - Defense in depth (slides)
  - Links (slides)
